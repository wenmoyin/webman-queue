<?php

namespace xunwu659\WebmanQueue\Queue\Factory;

use RedisException;
use xunwu659\WebmanQueue\Consumer;
use xunwu659\WebmanQueue\Interface\QueueConsumerInterface;
use xunwu659\WebmanQueue\Queue\QueueConsumer;
use Throwable;

class QueueConsumerFactory
{
    private static array $instances = [];

    /**
     * @throws RedisException
     * @throws Throwable
     */
    public static function create(string|Consumer $consumerClassOrObject, string $consumerName): QueueConsumerInterface
    {
        $instanceId = md5(is_object($consumerClassOrObject) ? get_class($consumerClassOrObject) : $consumerClassOrObject);
        if (!isset(self::$instances[$instanceId]) || !self::$instances[$instanceId] instanceof QueueConsumer) {
            self::$instances[$instanceId] = new QueueConsumer($consumerClassOrObject, $consumerName);
        }
        return self::$instances[$instanceId];
    }
}
