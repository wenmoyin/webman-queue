<?php

namespace xunwu659\WebmanQueue\Queue\Factory;

use xunwu659\WebmanQueue\Consumer;
use xunwu659\WebmanQueue\Interface\DelayedQueueInterface;
use xunwu659\WebmanQueue\Queue\AbstractQueueMember;
use xunwu659\WebmanQueue\Queue\DelayedQueue;

class DelayedQueueFactory
{
    private static array $instances = [];

    /**
     * @param AbstractQueueMember|Consumer $queueMember
     * @return DelayedQueue
     */
    public static function create(AbstractQueueMember|Consumer $queueMember): DelayedQueueInterface
    {
        $instanceId = spl_object_hash($queueMember);
        if (!isset(self::$instances[$instanceId]) || !self::$instances[$instanceId] instanceof DelayedQueue) {
            self::$instances[$instanceId] = new DelayedQueue(
                $queueMember->getDelayedTaskSetKey(),
                $queueMember->getDelayedDataHashKey(),
                $queueMember->getRedisConnection()
            );
        }
        return self::$instances[$instanceId];
    }
}
