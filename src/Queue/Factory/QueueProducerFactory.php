<?php

namespace xunwu659\WebmanQueue\Queue\Factory;


use RedisException;
use xunwu659\WebmanQueue\Consumer;
use xunwu659\WebmanQueue\Interface\QueueProducerInterface;
use xunwu659\WebmanQueue\Queue\QueueProducer;
use Throwable;

class QueueProducerFactory
{
    private static array $instances = [];

    /**
     * @throws RedisException
     * @throws Throwable
     */
    public static function create(string|Consumer $consumerClassOrObject): QueueProducerInterface
    {
        $instanceId = md5(is_object($consumerClassOrObject) ? get_class($consumerClassOrObject) : $consumerClassOrObject);
        if (!isset(self::$instances[$instanceId]) || !self::$instances[$instanceId] instanceof QueueProducer) {
            self::$instances[$instanceId] = new QueueProducer($consumerClassOrObject);
        }
        return self::$instances[$instanceId];
    }
}
