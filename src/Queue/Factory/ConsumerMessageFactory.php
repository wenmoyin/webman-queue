<?php

namespace xunwu659\WebmanQueue\Queue\Factory;

use xunwu659\WebmanQueue\ConsumerMessage;
use xunwu659\WebmanQueue\Interface\ConsumerMessageInterface;
use xunwu659\WebmanQueue\Queue\QueueConsumer;
use xunwu659\WebmanQueue\Queue\QueueMessage;

class ConsumerMessageFactory
{
    /**
     * @param string $messageId
     * @param array $message
     * @param QueueConsumer $queueConsumer
     * @return ConsumerMessageInterface
     */
    public static function create(string $messageId, array $message, QueueConsumer $queueConsumer): ConsumerMessageInterface
    {
        $queueMessage = QueueMessage::createFromArray($message);
        return new ConsumerMessage($messageId, $queueMessage, $queueConsumer);
    }
}
