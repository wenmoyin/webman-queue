<?php
return [
    xunwu659\WebmanQueue\Commands\MakeConsumerCommand::class,
    xunwu659\WebmanQueue\Commands\RemoveConsumerCommand::class,
    xunwu659\WebmanQueue\Commands\CleanRedisDataCommand::class,
    xunwu659\WebmanQueue\Commands\ConsumerListCommand::class
];
