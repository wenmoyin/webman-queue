<?php
return [
    'default' => [
        'host' => 'redis://'.getenv('REDIS_HOSTNAME').':'.getenv('PORT'),
        'options' => [
            'auth' => getenv('REDIS_PASSWORD'),     // 密码，字符串类型，可选参数
            'db' => getenv('SELECT'),           // 数据库
            'prefix' => 'webman_queue_',      // key 前缀
            'timeout' => 2, // Timeout
            'ping' => 55,             // Ping
            'reconnect' => true,  // 断线重连
            'max_retries' => 5, // 最大重连次数
            'retry_interval' => 5 , // 重连间隔 s
        ]
    ],
];
